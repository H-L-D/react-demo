import React, { Component } from 'react';
import Button from 'react-bootstrap/Button'
import './Results.css';

class Results extends Component {
    fight = () => {
        this.props.history.push('/fightpage');
    }

    home = () => {
        this.props.history.push('/');
    }

    render() {
        return (
            <div id="resultsBody" >
                <p id="winnerHeader">Winner</p>
                <img id="winnerPic" src={this.props.winner.src} alt='./images/empty.jpg' />
                <p id="winnerText">{this.props.winner.value}!</p>
                <div>
                    <Button id="resultsButtons" onClick={this.fight}>Fight Again</Button>
                    <Button id="resultsButtons" onClick={this.home}>Start Over</Button>
                </div>
            </div>
        )
    }
}

export default Results;