import React, { Component } from 'react';
import './fight.css';
import { withRouter } from 'react-router-dom'
import Button from 'react-bootstrap/Button'

class Fight extends Component {
    results = () => {
        this.props.pickWinner();
        this.props.history.push('/resultspage');
    }

    render() {
        return (
            <div id="body">
                <div>
                    <img className="fighter" src={this.props.pThreeObj.src} alt='./images/empty.jpg' />
                </div>
                <img className="fighter" src={this.props.pOneObj.src} alt='./images/empty.jpg' />
                <img className="fight" src='./images/Cat_Fight.gif' alt='./images/empty.jpg' />
                <img className="fighter" src={this.props.pTwoObj.src} alt='./images/empty.jpg' />
                <div><Button onClick={this.results} id="resultsButton" variant="success">Results</Button></div>
            </div>
        )
    }
}

export default withRouter(Fight);