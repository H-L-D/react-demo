import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import './Cat.css'

class Cat extends Component {
    render() {
        return (
            <Button variant="dark" className="catBox"
                style={{ backgroundImage: 'url(' + this.props.src + ')' }}
                onClick={() => this.props.onClick(this.props)}>
                {this.props.value}
            </Button>
        )
    }
}

export default Cat;