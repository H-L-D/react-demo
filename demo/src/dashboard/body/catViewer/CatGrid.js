import React, { Component } from 'react';
import './CatGrid.css';
import Cat from './Cat';

class CatGrid extends Component {
    constructor(props) {
        super(props);
        this.state = {
            catList: [
                { name: 'Chonky', src: './images/Chonky.jpg' },
                { name: 'Bullet', src: './images/Bullet.jpg' },
                { name: 'Darth Fluffy', src: './images/DarthFluffy.jpg' },
                { name: 'Pizza Time', src: './images/Pizza_time.png' },
                { name: 'Cat With Long Arms', src: './images/Cat_with_long_arms.jpg' },
                { name: 'Long Boi', src: './images/Long_boi.jpg' },
                { name: 'Nyon Cat', src: './images/Nyon_cat.jpg' },
                { name: 'TacOcaT', src: './images/TacOCat.jpg' },
                { name: 'Shadow', src: './images/Shadow.jpg' },
                { name: 'Polar Puss', src: './images/Polar_Puss.jpg' },
                { name: 'Impostor', src: './images/Impostor.jpg' },
                { name: 'Tiger', src: './images/Tiger.jpg' }
            ],
            showText: false
        };
    }

    render() {
        return (
            <div>
                <table className="text-center" id="catGrid">
                    <tr>
                        {this.state.catList && this.state.catList.map((cat) => {
                            return (
                                <Cat
                                    value={cat.name}
                                    src={cat.src}
                                    onClick={this.props.onClick}
                                />
                            )
                        })}
                    </tr>
                </table>
                {this.state.showText && <p>Hello world!</p>}
            </div>
        )
    }
}

export default CatGrid;