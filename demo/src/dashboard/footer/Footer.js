import React, { Component } from 'react';
import './Footer.css';
import Button from 'react-bootstrap/Button'
import { withRouter } from 'react-router-dom'

class Footer extends Component {
    fight = () => {
        if (this.props.pOneImg !== './images/empty.jpg' &&
            this.props.pTwoImg !== './images/empty.jpg' &&
            this.props.pThreeImg !== './images/empty.jpg') {
            this.props.history.push('/fightpage');
        }
    }

    render() {
        return (
            <div id="footer">
                <img class="selectedFighters" id="p1" src={this.props.pOneImg} alt='./images/empty.jpg' />
                VS
                <img class="selectedFighters" id="p2" src={this.props.pTwoImg} alt='./images/empty.jpg' />
                VS
                <img class="selectedFighters" id="p3" src={this.props.pThreeImg} alt='./images/empty.jpg' />                
                <Button onClick={this.fight} id="fightButton" variant="danger">Fight!</Button>
            </div>
        )
    }
}

export default withRouter(Footer);