import React, { Component } from 'react';
import NavbarComponent from './header/NavbarComponent.js';
import CatGrid from './body/catViewer/CatGrid.js';
import Footer from './footer/Footer.js';

class Dashboard extends Component {
    render() {
        return (
            <div>
                <NavbarComponent></NavbarComponent>
                <h1 className="text-center" id="header">Choose your fighter!</h1>
                <CatGrid
                    onClick={this.props.onClick}
                ></CatGrid>
                <Footer
                    pOneImg={this.props.pOneImg}
                    pTwoImg={this.props.pTwoImg}
                    pThreeImg={this.props.pThreeImg}
                ></Footer>
            </div >
        )
    }
}

export default Dashboard;