import React, { Component } from 'react';
import './App.css';
import Dashboard from './dashboard/Dashboard.js';
import Fight from './fightpage/fight.js';
import Results from './results/Results';
import { BrowserRouter as Router, Route } from "react-router-dom";

let pOneObj;
let pTwoObj;
let pThreeObj;
let nextFighter = 0;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pOneImg: "./images/empty.jpg",
      pTwoImg: "./images/empty.jpg",
      pThreeImg: "./images/empty.jpg",
      winner: { pOneObj }
    }
  }

  onClick = (src) => {
    if (nextFighter === 0) {
      this.setState({ pOneImg: src.src });
      pOneObj = src;
    } else if (nextFighter === 1) {
      this.setState({ pTwoImg: src.src });
      pTwoObj = src;
    } else {
      this.setState({ pThreeImg: src.src });
      pThreeObj = src;
    }
    nextFighter++;
    if (nextFighter === 3) {
      nextFighter = 0;
    }
  }

  pickWinner = () => {
    let rnd = ((Math.random() * 100) + 1)
    if (rnd < 33) {
      this.setState({ winner: pOneObj });
    } else if (rnd < 66 && rnd >= 33) {
      this.setState({ winner: pTwoObj });
    } else {
      this.setState({ winner: pThreeObj })
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
            crossorigin="anonymous"
          />
          <Router>
            <Route exact={true} path="/"
              render={(props) => <Dashboard {...props}
                onClick={this.onClick}
                pOneImg={this.state.pOneImg}
                pTwoImg={this.state.pTwoImg}
                pThreeImg={this.state.pThreeImg}
              />}
            />
            <Route exact={true} path="/fightpage"
              render={(props) => <Fight {...props}
                pOneObj={pOneObj}
                pTwoObj={pTwoObj}
                pThreeObj={pThreeObj}
                pickWinner={this.pickWinner}
                winner={this.state.winner}
              />}
            />
            <Route exact={true} path="/resultspage"
              render={(props) => <Results {...props}
                pOneObj={pOneObj}
                pTwoObj={pTwoObj}
                winner={this.state.winner}
              />}
            />
          </Router>
        </header>
      </div>
    );
  }
}

export default App;
